#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace ::cv;
using namespace ::std;

int main(int argc, char *argv[]) {
    assert(argc == 3);

    const string input_path = argv[1];
    const string output_path = argv[2];

    const Mat input = imread(input_path, CV_LOAD_IMAGE_GRAYSCALE);

    Mat output(input.size(), CV_8U);
    for (int i = 0; i < input.rows; ++i) {
        for (int j = 0; j < input.cols; ++j) {
            output.at<unsigned char>(i, j) = 255 - input.at<unsigned char>(i, j);
        }
    }

    string input_window = "Input";
    string output_window = "Output";

    imshow(input_window, input);
    imshow(output_window, output);
    const int offset = 100;
    moveWindow(input_window, offset, offset);
    moveWindow(output_window, input.cols + offset, offset);

    imwrite(output_path, output);

    waitKey();
}
